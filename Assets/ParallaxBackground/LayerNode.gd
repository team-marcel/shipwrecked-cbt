extends Node2D

var camera
var speed = 0
var lastPosition

# Called when the node enters the scene tree for the first time.
func _ready():
	camera = get_node("/root/Game/Player/Camera2D")
	lastPosition = camera.global_position


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _physics_process(delta):
	var movement = Vector2(camera.global_position) - lastPosition
	self.position.x += movement.x * speed
	self.position.y += movement.y * speed
	lastPosition = camera.global_position
