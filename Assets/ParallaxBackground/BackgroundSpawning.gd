extends Node2D

var rng = RandomNumberGenerator.new()
onready var layerNodeObject = load("res://Assets/ParallaxBackground/LayerNode.tscn")
var amount = 50000
#Dictionary wird für die verschiedenen Ebenen genutzt, weil man eigene Klassen(/Instanzen) leider nicht
#als export var benutzen kann, daher muss falls man mehr als 4 Layer benötigt leider auch der Block 
#innerhalb des Dictionarys kopiert werden.
export var layers = [{
	"ObjectSize": 100.0,
	"ZIndex": -1,
	"RandomRot": false,
	"Amount": 0,
	"ParallaxNearPlayer(0.0-1.0)": 0.1,
	"SpawnRange": 50000,
	".tscn Object": []
}, {
	"ObjectSize": 100.0,
	"ZIndex": -1,
	"RandomRot": false,
	"Amount": 0,
	"ParallaxNearPlayer(0.0-1.0)": 0.1,
	"SpawnRange": 50000,
	".tscn Object": []
}, {
	"ObjectSize": 100.0,
	"ZIndex": -1,
	"RandomRot": false,
	"Amount": 0,
	"ParallaxNearPlayer(0.0-1.0)": 0.1,
	"SpawnRange": 50000,
	".tscn Object": []
}, {
	"ObjectSize": 100.0,
	"ZIndex": -1,
	"RandomRot": false,
	"Amount": 0,
	"ParallaxNearPlayer(0.0-1.0)": 0.1,
	"SpawnRange": 50000,
	".tscn Object": []
}]

# Called when the node enters the scene tree for the first time.
func _ready():
	#Lade das Layer-Objekt, eine "leere" Node, die nur das LayerNode-Script hat, dient dazu, dass die ganzen Objekte
	#die auf der jeweiligen Layer erschaffen werden als Child-Objekt des LayerNode-Objektes eingefügt werden, um nur
	#das Parent Objekt bewegen zu müssen, um die ganze Layer zu verschieben.
	layerNodeObject = load("res://Assets/ParallaxBackground/LayerNode.tscn")
	#Durchläuft jede Layer und erschafft für sie ein LayerNode-Objekt
	for m in layers.size():
		var layerNodeObjectInstance = layerNodeObject.instance()
		add_child(layerNodeObjectInstance)
		#Es werden nur Layer berücksichtigt, auf denen auch Objekte gespawned werden sollen, um unnötige Rechenarbeit zu
		#ersparen.
		if layers[m]["Amount"] > 0:
			#Erschafft so oft wie in der Variable "Amount" der jeweiligen Layer eingestellt zufällig eines
			#der festgelegten Objekte an zufälliger Position und falls aktiviert mit zufälliger Drehung
			for n in layers[m]["Amount"]:
				rng.randomize()
				var randomObject = rng.randi_range(0, layers[m][".tscn Object"].size() - 1)
				var objectToSpawn = layers[m][".tscn Object"][randomObject]
				var spawnObjectInstance = objectToSpawn.instance()
				var randomXPos = rng.randf_range(-layers[m]["SpawnRange"], layers[m]["SpawnRange"])
				var randomYPos = rng.randf_range(-layers[m]["SpawnRange"], layers[m]["SpawnRange"])
				spawnObjectInstance.position.x = randomXPos
				spawnObjectInstance.position.y = randomYPos
				spawnObjectInstance.scale = Vector2(layers[m]["ObjectSize"] / 100.0, layers[m]["ObjectSize"] / 100.0)
				if(layers[m]["RandomRot"]):
					var randomRotation = rng.randf_range(0.0, 360.0)
					spawnObjectInstance.rotation = randomRotation
				layerNodeObjectInstance.speed = layers[m]["ParallaxNearPlayer(0.0-1.0)"]
				spawnObjectInstance.z_index = layers[m]["ZIndex"]
				layerNodeObjectInstance.add_child(spawnObjectInstance)

