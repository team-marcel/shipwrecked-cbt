extends Node2D

export(Array, PackedScene) var spawnableResources
export(NodePath) var spawnpointsPath
onready var spawnpointsObj = get_node(spawnpointsPath)
onready var spawnpoints = spawnpointsObj.get_children()
export(int) var minNumber = 1
export(int) var maxNumber = 5
export(PackedScene) var spawnableEnemy
export(float) var enemySpawnChance = 5.0
export(int) var triesToSpawnEnemy = 7

func _ready():
	#Zufallszahl Setup
	var rng = RandomNumberGenerator.new()
	rng.randomize()
	
	#Würfeln der Menge an Ressourcen, die spawnen sollen
	var amount = rng.randi_range(minNumber, maxNumber)
	
	#Spawnen der Ressourcen
	for i in range(amount):
		var selectedResource = rng.randi_range(0, spawnableResources.size() - 1)
		var selectedSpawnpoint = spawnpoints[rng.randi_range(0, spawnpoints.size() - 1)]
		spawnpoints.erase(selectedSpawnpoint)
		var resourceInst = spawnableResources[selectedResource].instance()
		selectedSpawnpoint.add_child(resourceInst)
	
	#Spawnen der Gegner
	rng.randomize()
	for i in range(triesToSpawnEnemy):
		var randomChance = rng.randf_range(0.0, 100.0)
		if(randomChance <= enemySpawnChance):
			var enemyInst = spawnableEnemy.instance()
			var selectedSpawnpoint = spawnpoints[rng.randi_range(0, spawnpoints.size() - 1)]
			spawnpoints.erase(selectedSpawnpoint)
			#enemyInst.position = selectedSpawnpoint.position
			selectedSpawnpoint.add_child(enemyInst)

