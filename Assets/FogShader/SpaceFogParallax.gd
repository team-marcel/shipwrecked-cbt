extends Sprite

export(NodePath) var referenceObjectPath
onready var referenceObject = get_node(referenceObjectPath)
export(float) var followObjectAmount = 0.85
var lastPosition
var shaderOffset = Vector2(0.0, 0.0)

func _ready():
	if is_instance_valid(referenceObject):
		lastPosition = referenceObject.global_position
	else:
		lastPosition = Vector2(0,0)


func _process(delta):
	if is_instance_valid(referenceObject):
		var movement = referenceObject.global_position - lastPosition
		shaderOffset += followObjectAmount * movement * 1.0/512.0
		material.set_shader_param("offset", shaderOffset)
		lastPosition = referenceObject.global_position
	else:
		var movement = Vector2(0,0) - lastPosition
		shaderOffset += followObjectAmount * movement * 1.0/512.0
		material.set_shader_param("offset", shaderOffset)
		lastPosition = Vector2(0,0)
