extends Node2D

export var waveSpawnerActive = false
#Timer Stuff
export var timeBetweenWaves = 180.0
export(float) var addedTimeEachWave = 30.0
var currentTime = 0
#Gegner
export var spawnDistance = 1000.0
export var gegnerTSCN = []
export var maxProbValue = 300
export(Array, float) var gegnerProbabilty = [80]
export(bool) var decideRandomly = true
var enemyAmount = 10
export var startEnemyAmount = 5
var enemyAmountFloat = 0.0
export var enemyMultiplier = 1.05
export var enemiesAddedPerWave = 2
var waveActive = false
var activeEnemies = []
#Setup
export(NodePath) var spaceship
var rng = RandomNumberGenerator.new()
export(NodePath) var waveText
#onready var waveTextObj = get_node(waveText)
export(NodePath) var waveProgress
onready var waveProgressInst = get_node(waveProgress)
export(PackedScene) var krakenBackgroundDecoration
export(NodePath) var fireworksSpawnerPath
onready var fireworksSpawner = get_node(fireworksSpawnerPath)
var krakenSpawnTimer = 0.0
var nextKrakenSpawn = 5.0
var gameOver = false
var endedWaveEarlier = false
var endedWaveResetTimer = 0.0
var gameOverTimer = 0.0

#Audio
var wave_incomming = preload("res://Assets/Sound/Homeship/wave_incomming.mp3")
var wave_completed = preload("res://Assets/Sound/Homeship/wave_completed_ovox.mp3")
var backgroundMusic = preload("res://Assets/Sound/Music/BackgroundMusicShipwrecked.mp3")
var combatMusic = preload("res://Assets/Sound/Music/ShipwreckedCombatMusic.mp3")
onready var audioManager = $"../AudioManager"

var current_scene

func _ready():
	#Endscreen Loading
	var root = get_tree().get_root()
	current_scene = root.get_child(root.get_child_count() - 1)
	enemyAmount = startEnemyAmount
	enemyAmountFloat = float(enemyAmount)
	UserInterface.WaveSpawner = self
	nextKrakenSpawn = rng.randf_range(0.05, 0.25)

func _physics_process(delta):
	#Überprüfe, wie viele Gegner am Leben sind
	var allEnemies = get_children()
	var activeOnes = [] + allEnemies
	for enemy in allEnemies:
		if(enemy.dead):
			activeOnes.erase(enemy)
	activeEnemies = activeOnes
	
	var wasWaveActive = waveActive
	
	#Im Hintergrund während der Welle immer wieder Kraken erscheinen lassen
	if(activeEnemies.size() != 0):
		waveActive = true
		krakenSpawnTimer += delta
		if(krakenSpawnTimer >= nextKrakenSpawn):
			krakenSpawnTimer = 0.0
			nextKrakenSpawn = rng.randf_range(0.05, 0.25)
			var krakenDecInst = krakenBackgroundDecoration.instance()
			get_tree().root.add_child(krakenDecInst)
	else:
		waveActive = false
	
	#Play Music
	if(waveActive and audioManager.get_child(0).stream != combatMusic):
		audioManager.get_child(0).set_stream(combatMusic)
		audioManager.get_child(0).play(0)
	elif(!waveActive and audioManager.get_child(0).stream != backgroundMusic):
		audioManager.get_child(0).set_stream(backgroundMusic)
		audioManager.get_child(0).play(0)
	
	#Wave Completed Feedback
	if(wasWaveActive == true and waveActive == false and !endedWaveEarlier):
		UserInterface.Message.UIMessage("Du hast es geschafft, die Ratten dieser Welle sind vernichtet!", 5.0)
		audioManager.playAudio(wave_completed)
	
	#Zurücksetzen von endedWaveEarlier
	if(endedWaveEarlier):
		endedWaveResetTimer += delta
		if(endedWaveResetTimer >= 0.2):
			endedWaveEarlier = false
	
	#print("Current Active Enemies: ", activeEnemies.size())
	if(!gameOver):
		if(activeEnemies.size() == 0):
			currentTime += delta
			if(currentTime >= timeBetweenWaves && waveSpawnerActive):
				currentTime = 0
				spawnWave()
			waveProgressInst.value = (timeBetweenWaves - currentTime) / timeBetweenWaves * 100.0
		else:
			waveProgressInst.value = int(float(activeEnemies.size()) / float(enemyAmount) * 100.0)
	else:
		UserInterface.GrayscaleActive(1.5 * delta)
		gameOverTimer += delta
		if(gameOverTimer >= 8.0):
			gameOverTimer = 0.0
#			var level = get_tree().root.get_node("Game")
#			get_tree().remove_child(level)
#			level.call_deferred("free")
#			var endScreen = load("res://Scnenes/Endscreen.tscn")
#			get_tree().add_child(endScreen.instance())
			current_scene.queue_free()
			var new_scene = ResourceLoader.load("res://Scnenes/Endscreen.tscn")
			current_scene = new_scene.instance()
			get_tree().get_root().add_child(current_scene)
			get_tree().set_current_scene(current_scene)
#			var newSceneInst = new_scene.instance()
#			get_tree().root.add_child(newSceneInst)
			
			#PROBLEM: Wechseln der Szene funktioniert nicht ganz, es wird zwar die neue
			#			Szene reingemacht, aber die alte bleibt da, man sieht noch Teile
			#			des UI's und hört Geräusche etc.
	
	#Zoom Key (Oder was auch immer später vielleicht mal dafür eingestellt wird) sorgt dafür, dass die Welle
	#sofort spawned, anstatt wenn die Zeit abgelaufen ist
	if(Input.is_action_just_pressed("zoom") and activeEnemies.size() == 0 and !gameOver):
		spawnWave()
		fireworksSpawner.launchFireworks()

func spawnWave():
	rng.randomize()
	UserInterface.Message.UIMessage("Die Welle erscheint jetzt, verteidige das Raumschiff!", 4)
	currentTime = 0.0
	audioManager.playAudio(wave_incomming)
	enemyAmount = int(enemyAmountFloat)
	var spaceshipPos = get_node(spaceship)
	for i in enemyAmount:
		var enemyInstance
		if(decideRandomly):
			#Es wird ein zufälliger Wert zwischen 0 und dem im Inspector festgelegten maxProbValue Wert gewählt
			#Es wird der Gegner gewählt, dessen Wahrscheinlichkeit den höchsten Wert hat UND dessen 
			#Wahrscheinlichkeit höher als die gewürfelte Zahl ist.
			#Beispiel: 
			#maxProbValue = 300
			#gegnerProbability[0] = 0
			#gegnerProbability[1] = 200
			#Ist die gewürfelte Zahl zwischen 0 und 300 nun kleiner als 200, wird Objekt 0 gewählt, ist der Wert
			#größer als 200, wird Objekt 1 gewählt. Objekt 0 ist hier also doppelt so wahrscheinlich wie Objekt 1.
			var chosen = -1
			var chosenProb = 0
			var randomNumber = rng.randf_range(0, maxProbValue)
			for x in gegnerProbabilty.size():
				if(randomNumber >= gegnerProbabilty[x] && chosenProb <= gegnerProbabilty[x]):
					chosen = x
					chosenProb = gegnerProbabilty[x]
			#Wurde kein Wert gewählt, breche die Funktion ab, es wird nichts gespawned.
			if(chosen == -1): 
				return
			
			enemyInstance = gegnerTSCN[chosen].instance()
			print("Enemy spawned: " + str(enemyInstance.name))
			
		else:
			var current = i * (maxProbValue / enemyAmount)
			var chosen = 0
			for x in gegnerProbabilty.size():
				if(gegnerProbabilty[x] <= current):
					chosen = x
			enemyInstance = gegnerTSCN[chosen].instance()
			print("Enemy spawned: " + str(enemyInstance.name))
			
		#Erzeugen einer zufälligen Rotation, um eine "Sphäre" um das Spaceship zu erstellen, auf der
		#die Gegner spawnen. Die Distanz zum Spaceship wird über die spawnDistance Variable geregelt.
		var randomRot = rng.randf_range(0.0, 360.0)
		enemyInstance.spaceshipPos = spaceshipPos
		#Gegner werden mit leicht zufälligem Abstand gespawned, damit nicht alle exakt zeitgleich am
		#Spaceship ankommen
		var spawnDistVariation = rng.randf_range(0.95, 1.05)
		enemyInstance.position = spaceshipPos.position + Vector2(1.0,0.0).rotated(randomRot) * spawnDistance * spawnDistVariation
		add_child(enemyInstance)
		if(i == enemyAmount - 1):
			print("Enemies Spawned:", enemyAmount)
	enemyAmountFloat += enemiesAddedPerWave
	enemyAmountFloat *= enemyMultiplier
	timeBetweenWaves += addedTimeEachWave

func endWave(onlyShield):
	endedWaveEarlier = true
	if(onlyShield):
		for device in Input.get_connected_joypads():
			Input.start_joy_vibration(device, 0, 1, 5)
		UserInterface.Message.UIMessage("Der Schild wurde zerstört. Diesmal bist du noch davongekommen, aber bereite dich nächstes Mal besser vor!", 5.0)
		audioManager.playAudio(wave_completed)
		for enemy in get_children():
			enemy.damage(1000000)
			currentTime = 0.0
	else:
		for device in Input.get_connected_joypads():
			Input.start_joy_vibration(device, 0, 1, 5)
		UserInterface.Message.UIMessage("Dein Raumschiff wurde zerstört, du hast endgültig verloren.", 5.0)
		for enemy in get_children():
			enemy.damage(1000000)
		gameOver = true
		#Hier evtl. noch anderen Krams machen, wegen Game Over, andere Szene laden oder so
