extends KinematicBody2D

var spaceship
var spaceshipPos
var health = 100
export(float) var unitHealth = 100
var moveVector
export(float) var speed = 3000
var currentSpeed
var dead = false
export(String) var moveAnimation = "RatMovement"
export(String) var deathAnimation = "RatDeath"
export(PackedScene) var damageParticles
export(PackedScene) var shieldHitParticles
export(PackedScene) var deathParticles
export(float) var returnFromBounce = 0.05
export(float) var bounceStrength = 4.0
export(float) var damageDealt = 35.0
var deathTimer = 0.0

#Electrification
var electrified = false
var electrificationLength = 10.0
var electrificationTimer = 0.0

#Audio
var rat_electric = preload("res://Assets/Sound/Ratten/rat_electric.wav")
var rat_hurt = preload("res://Assets/Sound/Ratten/rat_hurt.wav")
var rat_bounce = preload("res://Assets/Sound/Ratten/rat_bounce.wav")
var playSound = preload("res://Assets/Sound/playAudio.tscn")
var hitSounds = [
	preload("res://Assets/Sound/Ratten/ratten_1.mp3"),
	preload("res://Assets/Sound/Ratten/ratten_2.mp3"),
	preload("res://Assets/Sound/Ratten/ratten_3.mp3"),
	preload("res://Assets/Sound/Ratten/ratten_4.mp3"),
	preload("res://Assets/Sound/Ratten/ratten_5.mp3"),
	preload("res://Assets/Sound/Ratten/ratten_6.mp3")
]

func _ready():
	#spaceshipPos = get_node(spaceship)
	moveVector = self.position.direction_to(spaceshipPos.position)
	health = unitHealth
	currentSpeed = speed	
	pass

func _process(delta):
	if(!dead):
		if(self.position.distance_to(spaceshipPos.position) > 120):
			moveVector = lerp(moveVector, self.position.direction_to(spaceshipPos.position), returnFromBounce)
			var collision = move_and_collide(moveVector * currentSpeed * delta)
			if collision:
				moveVector = moveVector.bounce(collision.normal).normalized() * bounceStrength
				if(collision.collider.name == "Shield_SB"):
					collision.collider.get_parent().damage(damageDealt)
					var hitParticlesInst = shieldHitParticles.instance()
					get_tree().root.add_child(hitParticlesInst)
					hitParticlesInst.position = collision.position
					hitParticlesInst.emitting = true
				if(collision.collider.name == "Weapon1_KB"):
					var damageToTake = collision.collider.get_parent().get_parent().weapon1Damage
					self.damage(damageToTake)
		self.look_at(self.position + moveVector)
		if(moveAnimation != ""):
			$AnimationPlayer.play(moveAnimation)
		#damage(10.0 * delta)
		#Nur für Testzwecke:
		#if(Input.is_action_pressed("tasteq")):
			#damage(70 * delta)
		
		#Electrification
		if(electrified):
			electrificationTimer += delta
			if(electrificationTimer >= electrificationLength):
				electrified = false
			currentSpeed = speed / 3.0
			if randi() % 2:
				$Sprite.modulate.a = 20.0
			else:
				$Sprite.modulate.a = 1.0
		else:
			$Sprite.modulate.a = 1.0
			currentSpeed = speed
			
	else:
		deathTimer += delta
		if(deathTimer < 0.3 and deathAnimation != ""):
			$AnimationPlayer.play(deathAnimation)
			$Sprite.modulate.a = 1.0
		if(deathTimer >= 5):
			$Sprite.modulate.a = lerp($Sprite.modulate.a, 0.2, 0.4 * delta)
		if(deathTimer >= 30):
			queue_free()

func damage(amount):
	health -= amount
	if not electrified:
		var rng = RandomNumberGenerator.new()
		rng.randomize()
		var picked = rng.randi_range(0, hitSounds.size() - 1)
		playAudio(hitSounds[picked])
	if(!dead):
		var damageParticleInstance = damageParticles.instance()
		add_child(damageParticleInstance)
		damageParticleInstance.emitting = true
	if(health < 0):
		health = 0
	if(health == 0 && !dead):
		#if(deathParticles != null):
		#	var deathParticleInstance = deathParticles.instance()
		#	add_child(deathParticleInstance)
		#	deathParticleInstance.emitting = true
		for i in range(0, 8):
			var damageParticleInst = damageParticles.instance()
			add_child(damageParticleInst)
			damageParticleInst.emitting = true
		dead = true
		$CollisionShape2D.disabled = true
		$Sprite.modulate.a = 1.0

func electrify(length):
	electrified = true
	electrificationTimer = 0.0

#Wird gebraucht, um zu erkennen, ob ein Objekt dieses Skript hat, bitte nicht entfernen!
func waveEnemy():
	pass
	
func playAudio(audio):
	var sound = playSound.instance()
	add_child(sound)
	sound.playAudioFunc(audio)
