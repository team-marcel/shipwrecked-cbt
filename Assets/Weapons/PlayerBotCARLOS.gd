extends Node2D

export(NodePath) var playerPath
onready var player = get_node(playerPath)
export(Vector2) var positionOffset = Vector2(15.0, -30.0)
var designatedPosition
export(float) var followSpeed = 0.07
export(PackedScene) var missile
var joyInput = Vector2(0, 0)
var lastKnownDirection = Vector2(1.0, 0)
var cursorDirection = Vector2(1.0, 0)
#cooldown
var counter = 0
var cooldown = true
#audio
onready var audioManager = $"../AudioManager"
var shot = preload("res://Assets/Sound/CARLOS/carlos_shot.mp3")

var glowCounter = 0
var updown = true
onready var glowSprite = $Carlos/Sprite_Glow

func _process(delta):
	if updown == true:
		if glowCounter >= 1.5:
			updown = false
		else:
			glowCounter += 0.02
	else:
		if glowCounter <= 1:
			updown = true
		else:
			glowCounter -= 0.02
	$Carlos/Sprite_Glow.modulate.a = glowCounter

func _physics_process(delta):
	if(Input.get_connected_joypads().size() > 0 and !UserInterface.usingMouse):
			var device = Input.get_connected_joypads()[0]
			joyInput = Vector2(Input.get_joy_axis(device, JOY_AXIS_2), (Input.get_joy_axis(device, JOY_AXIS_3)))
			if(joyInput.length() >= 0.2):
				cursorDirection = joyInput
	else:
		cursorDirection = (get_global_mouse_position() - global_position).normalized()
	$Carlos/Cursor.rotation = cursorDirection.normalized().rotated(PI / 2).angle()
	
	counter += delta
	if counter > 0.5 and cooldown == true:
		cooldown = false
		counter = 0
	if(player.get_child(0).flip_h):
		designatedPosition = player.global_position + positionOffset
		$Carlos/Sprite.flip_h = true
		$Carlos/Sprite_Glow.flip_h = true
	else:
		designatedPosition = player.global_position + Vector2(-positionOffset.x, positionOffset.y)
		$Carlos/Sprite.flip_h = false
		$Carlos/Sprite_Glow.flip_h = false
	if(player.inHomeShip):
		modulate.a = lerp(modulate.a, 0.0, 12 * delta)
	else:
		modulate.a = lerp(modulate.a, 1.0, 12 * delta)
		#Joystick Input bekommen
		if(Input.get_connected_joypads().size() > 0 and !UserInterface.usingMouse):
			var device = Input.get_connected_joypads()[0]
			joyInput = Vector2(Input.get_joy_axis(device, JOY_AXIS_2), (Input.get_joy_axis(device, JOY_AXIS_3)))
			if(joyInput.length() >= 0.2):
				lastKnownDirection = joyInput
		#Rakete Abfeuern
		if(Input.is_action_just_pressed("use_weapon")):
			if cooldown == false:
				for device in Input.get_connected_joypads():
					#device, Value Weak Motor, Value Strong Motor, Time - float zwischen 0 und 1 pls
					Input.start_joy_vibration(device, 0, 0.6, 0.15)
				UserInterface.CameraShake("small")
				var missileInst = missile.instance()
				get_tree().root.add_child(missileInst)
				missileInst.position = global_position
				var mouseVector
				if(UserInterface.lastUsed == "Mouse"):
					mouseVector = (get_global_mouse_position() - global_position).normalized()
				else:
					mouseVector = lastKnownDirection.normalized()
				missileInst.directionAngle = mouseVector
				audioManager.playAudio(shot)
				cooldown = true
				counter = 0.0
	
	position = lerp(position, designatedPosition, followSpeed * delta * 60.0)
	
