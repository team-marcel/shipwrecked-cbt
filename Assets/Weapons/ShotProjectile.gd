extends KinematicBody2D

var directionAngle = Vector2(0.0, 0.0)
var speed = 5.0
var damage = 10.0

var timeTillResolve = 6.0
var timer = 0.0

#Streuung
var rng = RandomNumberGenerator.new()
export(float) var spread = 7.0
var thisSpread

func _ready():
	rng.randomize()
	thisSpread = rng.randf_range(-spread, spread)


func _physics_process(delta):
	var collision = move_and_collide(directionAngle.rotated(deg2rad(thisSpread)) * delta * speed * 130.0)
	if(collision):
		if(collision.collider.has_method("waveEnemy")):
			collision.collider.damage(damage)
			print("Enemy hit by Projectile")
			queue_free()
	
	#Objekt nach Überschreiten der Zeit entfernen
	timer += delta
	if(timer >= timeTillResolve):
		queue_free()
