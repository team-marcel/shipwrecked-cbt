extends KinematicBody2D

var directionAngle = Vector2(0.0, 0.0)
export(float) var speed = 102.0
export(float) var damage = 40.0
var target = null
export(PackedScene) var explosionParticles
export(PackedScene) var explosionParticlesOther

var timer = 0.0
var lifetime = 20.0 #Zeit nach der die Rakete verschwindet

#audio
var hit = preload("res://Assets/Sound/CARLOS/bullet_hit.mp3")
var shot = preload("res://Assets/Sound/CARLOS/carlos_shot.mp3")
var playSound = preload("res://Assets/Sound/playAudio.tscn")

func _process(delta):
	timer += delta
	if(timer >= lifetime):
		timer = 0.0
		queue_free()
	if(timer >= (lifetime - 1.0)):
		scale = lerp(scale, Vector2(0.0, 0.0), 8.5 * delta)
		modulate.a = lerp(modulate.a, 0.0, 8.5 * delta)
	var changedDirectionAngle = directionAngle.rotated(0.15 * sin(5 * timer)) 
	var collision = move_and_collide(changedDirectionAngle * delta * speed)
	rotation = changedDirectionAngle.angle()
	if(collision):
		if(collision.collider.has_method("roamingEnemy")):
			collision.collider.damage(damage)
			print("Enemy hit by Bot Missile")
			var explosionInst = explosionParticles.instance()
			get_tree().root.add_child(explosionInst)
			explosionInst.position = collision.position
			explosionInst.emitting = true
		else:
			var explosionInst = explosionParticlesOther.instance()
			get_tree().root.add_child(explosionInst)
			explosionInst.position = collision.position
			explosionInst.emitting = true
		if(global_position.distance_to(UserInterface.Player.global_position) <= 285.0):
				UserInterface.CameraShake("big")
				playAudio(hit)
				for device in Input.get_connected_joypads():
					#device, Value Weak Motor, Value Strong Motor, Time - float zwischen 0 und 1 pls
					Input.start_joy_vibration(device, 0, 0.6, 0.2)
		queue_free()
	#Find targets
	var allInRange = $Area2D.get_overlapping_bodies()
	for eachObject in allInRange:
		if(eachObject.has_method("roamingEnemy")):
			if(target == null):
				target = eachObject
				print("CARLOS: Ziel gefunden!")
			else:
				if(eachObject.global_position.distance_to(global_position) < target.global_position.distance_to(global_position)):
					target = eachObject
	if(target != null):
		if is_instance_valid(target):
			directionAngle = (target.global_position - global_position).normalized()

func playAudio(audio):
	var sound = playSound.instance()
	get_tree().root.add_child(sound)
	sound.playAudioFunc(audio)
