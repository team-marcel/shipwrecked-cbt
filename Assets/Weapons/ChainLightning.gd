extends Line2D

export(float) var lifespan = 0.09
var timer = 0.0

func _ready():
	pass

#Zerstörung des Objektes nach der lifespan
func _process(delta):
	timer += delta
	if(timer >= lifespan):
		queue_free()
