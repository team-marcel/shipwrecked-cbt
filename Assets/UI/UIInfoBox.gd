extends Sprite


var currentSize = 0.01
var currentTransparency = 0.0
var hide = false
var maxSize = 0.65
var useDuration = false
var duration = 100.0
var timer = 0.0

#audio
var open = preload("res://Assets/Sound/UI/ui_open.mp3")
var playSound = preload("res://Assets/Sound/playAudio.tscn")

func _ready():
	playAudio(open)
	scale.x = currentSize
	scale.y = currentSize
	modulate.a = currentTransparency

func _process(delta):
	#Beim Erscheinen:
	if(!hide):
		currentSize = lerp(currentSize, maxSize, delta * 18)
		currentTransparency = lerp(currentTransparency, 0.9, delta * 8)
	else: #Beim Verschwinden
		currentSize = lerp (currentSize, -0.05, delta * 18)
		currentTransparency = lerp(currentTransparency, 0.0, delta * 8)
		
	scale.x = currentSize
	scale.y = currentSize
	modulate.a = currentTransparency
	
	if(useDuration):
		timer += delta
		if(timer >= duration):
			timer = 0.0
			hideBox()
			useDuration = false
	
	#Objekt entfernen, wenn verschwunden
	if(hide and currentSize <= 0.0):
		get_parent().currentInfoBox = null
		queue_free()

#Wird vom UI-Verwalterobjekt aufgerufen
func hideBox():
	hide = true
	
func playAudio(audio):
	var sound = playSound.instance()
	add_child(sound)
	sound.playAudioFunc(audio)
