extends Sprite

var messageDuration = 1000.0
var timer = 0.0
var currentSize = 0.01
var currentTransparency = 0.0

func _ready():
	scale.x = currentSize
	scale.y = currentSize
	modulate.a = currentTransparency


func _process(delta):
	timer += delta
	if(timer <= 1.0):
		currentSize = lerp(currentSize, 1.0, delta * 10)
		currentTransparency = lerp(currentTransparency, 0.9, delta * 8)
		#print("Changing Size: ", currentSize)
	
	if(timer >= messageDuration - 1.0):
		currentSize = lerp (currentSize, 0.0, delta * 10)
		currentTransparency = lerp(currentTransparency, 0.0, delta * 8)
	
	scale.x = currentSize
	scale.y = currentSize
	modulate.a = currentTransparency
	#maybe Transparenz auch? -> Marcel war hier lol
	
	if(timer >= messageDuration):
		queue_free()
	
