extends Node

var Message
var Player
var WaveSpawner
var Cam
var grayscaleEffect
#usingMouse, usingController und lastUsed werden in der physics_process ständig aktualisiert
#können also von anderen Scripts einfach abgefragt werden, um zu erkennen, ob gerade Maus oder
#Controller bewegt wird bzw. welches von beiden zuletzt bewegt wurde.
var usingMouse = false
var usingController = false
var lastMousePos = Vector2(0.0, 0.0)
var lastControllerPos = Vector2(0.0, 0.0)
var lastUsed = "Mouse"


func _ready():
	pass

func _physics_process(delta):
	#Maus-Bewegungs-Erkennung
	var currentMousePos = get_viewport().get_mouse_position()
	if(currentMousePos != lastMousePos):
		usingMouse = true
	else:
		usingMouse = false
	lastMousePos = currentMousePos
	#Controller-Bewegungs-Erkennung
	if(Input.get_connected_joypads().size() > 0):
		var device = Input.get_connected_joypads()[0]
		var currentControllerPos = Vector2(Input.get_joy_axis(device, JOY_AXIS_2), (Input.get_joy_axis(device, JOY_AXIS_3)))
		if(currentControllerPos != lastControllerPos):
			usingController = true
		else:
			usingController = false
		lastControllerPos = currentControllerPos
	else:
		usingController = false
	#print("Maus: " + str(usingMouse) + ", Controller: " + str(usingController))
	if(usingController):
		lastUsed = "Controller"
	elif(usingMouse):
		lastUsed = "Mouse"

func CameraShake(mode):
	if mode == "big":
		#strength, speed, decay
		Cam.doShake(10,20, 4.5) #großer Shake
	elif mode == "small":
		Cam.doShake(3, 20, 5.5) #kleiner Shake

func GrayscaleActive(speed=0.5):
	grayscaleEffect.material.set_shader_param("effectAmount", lerp(grayscaleEffect.material.get_shader_param("effectAmount"), 0.7, speed))
	
func GrayscaleInactive(speed=0.5):
	grayscaleEffect.material.set_shader_param("effectAmount", lerp(grayscaleEffect.material.get_shader_param("effectAmount"), 0.0, speed))
