extends Node2D

#Generation Method 1
export(float) var startPoints = 7
export(float) var distanceBetween = 1300
export(float) var minDistance = 500.0
var points = []
var rng = RandomNumberGenerator.new()
export(float) var steps = 10
export(Array, PackedScene) var objectsToSpawn

#Generation Method 2
export(float) var gen2TotalPoints = 1000
export(float) var gen2Dimensions = 20000.0

func _ready():
	generationMethod2()

func generationMethod2():
	rng.randomize()
	#Generate Random Positions
	for i in range(0, gen2TotalPoints):
		var genPoint = Vector2(rng.randf_range(-gen2Dimensions, gen2Dimensions), rng.randf_range(-gen2Dimensions, gen2Dimensions))
		if(!((genPoint.x > -500 and genPoint.x < 500) and (genPoint.y > -500 and genPoint.y < 500))):
			points.append(genPoint)
	
	#Check for too close neighbors
	var tooClose = []
	for point in points:
		var withoutSelf = [] + points
		withoutSelf.erase(point)
		for otherPoint in withoutSelf:
			if(point.distance_to(otherPoint) < minDistance):
				tooClose.append(point)
	
	for point in tooClose:
		points.erase(point)
		
	#Spawn Structures
	print("Spawning Structures: ", points.size())
	if(points.size() > 0):
		for eachPoint in points:
			var randomChosen = rng.randi_range(0, objectsToSpawn.size() - 1)
			var objectInst = objectsToSpawn[randomChosen].instance()
			objectInst.global_position = eachPoint
			add_child(objectInst)
	
func generationMethod1():
	rng.randomize()
	#Generating all Positions
	for startPoint in range(0, startPoints):
		var genPoint = newPoint(Vector2(0.0, 0.0), 300.0)
		points.append(genPoint)
		for newPoints in range(0, steps):
			var newGenPoint = newPoint(genPoint, minDistance)
			points.append(newGenPoint)
			genPoint = newGenPoint
	#print(points)
	
	#Spawn Structures
	if(points.size() > 0):
		for eachPoint in points:
			var randomChosen = rng.randi_range(0, objectsToSpawn.size() - 1)
			var objectInst = objectsToSpawn[randomChosen].instance()
			objectInst.global_position = eachPoint
			add_child(objectInst)


func inForbidden(point, checkDistance):
	if((point.x > -500 and point.x < 500) and (point.y > -500 and point.y < 500)):
		return true
	else:
		var checkedDist = 5000.0
		for checkPoint in points:
			var calcDist = point.distance_to(checkPoint)
			if(calcDist < checkedDist):
				checkedDist = calcDist
		if(checkedDist < checkDistance):
			return true
		else:
			return false

func newPoint(fromPoint, checkDistance):
	var randVec = Vector2(rng.randf_range(-1.0, 1.0), rng.randf_range(-1.0, 1.0)).normalized()
	var nPoint = fromPoint + (randVec * distanceBetween)
	if(inForbidden(nPoint, checkDistance)):
		nPoint = newPoint(fromPoint, checkDistance)
	return nPoint
