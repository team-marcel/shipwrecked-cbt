extends Node2D

export(int) var roaming_range = 100

onready var start_pos = global_position
onready var target_pos = global_position

onready var timer = $Timer

func _ready():
	 start_pos = global_position
	 target_pos = global_position

func update_target_position():
	var target_vector = Vector2(rand_range(- roaming_range, roaming_range), rand_range(- roaming_range, roaming_range))
	target_pos = start_pos + target_vector # Versuch für roaming (gibt eine roaming zone um die start pos an)
	return target_pos

func get_time_left():
	return timer.time_left # Gibt die Zeit an

func start_roaming_timer(duration):
	timer.start(duration)

func _on_Timer_timeout():
	update_target_position()
