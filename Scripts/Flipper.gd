extends Node2D

export(NodePath) var objectToFlip
onready var flipObject = get_node(objectToFlip)
export(NodePath) var objectWithRotation
onready var rotationObject = get_node(objectWithRotation)
export(bool) var isFlippedOnStart

func _ready():
	pass


func _process(delta):
	var rot = rotationObject.rotation
	var direction = Vector2(1, 0).rotated(rot)
	var directionX = direction.x
	if(directionX < 0):
		if(isFlippedOnStart):
			flipObject.flip_v = false
		else:
			flipObject.flip_v = true
	elif(directionX > 0):
		if(isFlippedOnStart):
			flipObject.flip_v = true
		else:
			flipObject.flip_v = false
	
	
