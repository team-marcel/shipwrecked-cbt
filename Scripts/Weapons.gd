extends Node2D

#UI/General Variables
export(NodePath) var playerPath
onready var player = get_node(playerPath)
#export(NodePath) var tabMenuPath
#onready var tabMenu = get_node(tabMenuPath)
export(NodePath) var weapon1UIPath
onready var weapon1UI = get_node(weapon1UIPath)
export(NodePath) var weapon2UIPath
onready var weapon2UI = get_node(weapon2UIPath)
export(NodePath) var weapon3UIPath
onready var weapon3UI = get_node(weapon3UIPath)
export(NodePath) var weaponSelectorPath
onready var weaponSelector = get_node(weaponSelectorPath)
export(NodePath) var shieldPath
onready var shield = get_node(shieldPath)
var weaponOffset = 0
var inSpaceship = false
var currentWeapon = 1
var currentFrame = 0

#Weapon Variables
export(NodePath) var weapon1Path
onready var weapon1 = get_node(weapon1Path)
export(float) var weapon1TurnSpeed = 0.2
var weapon1Active = false
export(float) var weapon1Damage = 10

export(NodePath) var weapon2Path
onready var weapon2 = get_node(weapon2Path)
export(float) var weapon2TurnSpeed = 0.2
export(float) var weapon2Damage = 10
var weapon2Cooldown = 0.0
export(float) var weapon2CooldownLength = 1.5
export(NodePath) var waveSpawnerPath
onready var waveSpawner = get_node(waveSpawnerPath)
export(float) var weapon2JumpDistance = 150.0
export(PackedScene) var weapon2ChainLightning
export(float) var weapon2ElectrificationLength = 2.0

export(NodePath) var weapon3Path
onready var weapon3 = get_node(weapon3Path)
export(float) var weapon3TurnSpeed = 0.2
export(float) var weapon3Damage = 10
export(float) var weapon3Cooldown = 0.15
var weapon3Timer = 0.0
export(PackedScene) var shotProjectileObj

var mouseAngle = 0.0

#Audio
var rat_electric = preload("res://Assets/Sound/Ratten/rat_electric.wav")
var laser_shoot = preload("res://Assets/Sound/Weapons/laser.mp3")
var shot = preload("res://Assets/Sound/Weapons/carlos_shot.wav") #Verbesserungswürdig alte ist:
# var shot = preload("res://Assets/Sound/shot_2.wav")
onready var audioManager = $"../../AudioManager"

#Controller
var leftStickAxis = Vector2(0,1)
var lastLeftStickAxis = Vector2(0,1)
var alternativeMode = false

func _ready():
	for device_id in Input.get_connected_joypads():
		print(Input.get_joy_name(device_id))

func _process(delta):
	if(shield.lives == 0 and shield.shipHealth == 0):
		return
	#Controller Input
	for device in Input.get_connected_joypads():
		var calculatedLeftStickAxis = Vector2(Input.get_joy_axis(device, JOY_AXIS_2)*-1, (Input.get_joy_axis(device, JOY_AXIS_3)*-1))
		#print(calculatedLeftStickAxis)
		if(!alternativeMode):
			if(calculatedLeftStickAxis.length() >= 0.1):
				leftStickAxis = calculatedLeftStickAxis
				lastLeftStickAxis = leftStickAxis
			else:
				leftStickAxis = lastLeftStickAxis
		else: #Alternative Mode funktioniert noch nicht, wird auch evtl. nicht eingebaut
			#if(calculatedLeftStickAxis.length() >= 0.1):
			leftStickAxis = leftStickAxis.rotated(calculatedLeftStickAxis.x * delta / 10.0)
			lastLeftStickAxis = leftStickAxis
			#else:
				#leftStickAxis = lastLeftStickAxis
		#print(leftStickAxis * 10) #die Multiplikation mit 10 ist nicht Notwendig,
		# ermöglicht aber eine Position weit weg vom Schiff, sodass mal einfach die Mouse position mit diesem Vector
		# ersetzen kann
	inSpaceship = player.inHomeShip
	currentFrame += 1
	if(currentFrame >= 60):
		currentFrame = 0
	weapon3Timer += delta
	#------Update UI und Waffen drehen------
	if(inSpaceship):
		if(Input.is_action_just_pressed("ui_right")):
			currentWeapon += 1
		elif(Input.is_action_just_pressed("ui_left")):
			currentWeapon -= 1
		if(currentWeapon < 1):
			currentWeapon = 3
		elif(currentWeapon > 3):
			currentWeapon = 1
		
		if(Input.is_action_just_pressed("Taste1")):
			currentWeapon = 1
		elif(Input.is_action_just_pressed("Taste2")):
			currentWeapon = 2
		elif(Input.is_action_just_pressed("Taste3")):
			currentWeapon = 3
		
		#Winkel zum Mauszeiger berechnen
		var mousePosition = get_global_mouse_position().normalized().rotated(PI / 2)
		leftStickAxis = leftStickAxis.normalized().rotated(-PI / 2)
		
		match currentWeapon:
			1:
				weaponSelector.position = weapon1UI.position
				
				var currentRot = weapon1.rotation
				var currentRotVec = Vector2(cos(currentRot), sin(currentRot)).normalized()
				var rotLerp
				if(Input.get_connected_joypads().size() > 0 and UserInterface.lastUsed == "Controller"):
					rotLerp = lerp(currentRotVec, leftStickAxis, weapon1TurnSpeed * delta * 10.0)
				else:
					rotLerp = lerp(currentRotVec, mousePosition, weapon1TurnSpeed * delta * 10.0)
				#print(leftStickAxis)
				var comparison = fposmod(rotLerp.angle()-currentRot + PI, PI*2) - PI
				if(comparison < -0):
					weapon1.get_child(0).flip_h = true
					weapon1.get_child(1).flip_h = true
					weapon1.get_child(2).position.x = -3
					weapon1.get_child(2).rotation_degrees = 180
				else:
					weapon1.get_child(0).flip_h = false
					weapon1.get_child(1).flip_h = false
					weapon1.get_child(2).position.x = 3
					weapon1.get_child(2).rotation_degrees = 0
				#print(rotLerp.angle())
				weapon1.rotation = rotLerp.angle()
			2:
				mousePosition = mousePosition.rotated(-PI / 2)
				weaponSelector.position = weapon2UI.position
				
				var currentRot = weapon2.rotation
				var currentRotVec = Vector2(cos(currentRot), sin(currentRot))
				var rotLerp
				if(Input.get_connected_joypads().size() > 0 and UserInterface.lastUsed == "Controller"):
					rotLerp = lerp(currentRotVec, leftStickAxis.rotated(-PI / 2), weapon2TurnSpeed * delta * 10.0)
				else:
					rotLerp = lerp(currentRotVec, mousePosition, weapon2TurnSpeed * delta * 10.0)
				weapon2.rotation = rotLerp.angle()
			3:
				mousePosition = mousePosition.rotated(-PI / 2)
				weaponSelector.position = weapon3UI.position
				
				var currentRot = weapon3.rotation
				var currentRotVec = Vector2(cos(currentRot), sin(currentRot))
				var rotLerp
				if(Input.get_connected_joypads().size() > 0 and UserInterface.lastUsed == "Controller"):
					rotLerp = lerp(currentRotVec, leftStickAxis.rotated(-PI / 2), weapon3TurnSpeed * delta * 10.0)
				else:
					rotLerp = lerp(currentRotVec, mousePosition, weapon3TurnSpeed * delta * 10.0)
				weapon3.rotation = rotLerp.angle()
				var comparison = fposmod(rotLerp.angle()-currentRot + PI, PI*2) - PI
				if(comparison < -0):
					weapon3.get_child(0).get_child(0).flip_v = true
				else:
					weapon3.get_child(0).get_child(0).flip_v = false
	#WeaponOffset berechnen, damit sie immer am äußersten Ring des Schilds befestigt sind
	match shield.activeLayers:
		1:
			weaponOffset = -10
		2:
			weaponOffset = -5
		3:
			weaponOffset = 0
	#Weapon1 (Kreissäge)
	weapon1.get_child(0).position.y = -157 - weaponOffset
	weapon1.get_child(1).position.y = -157 - weaponOffset
	weapon1.get_child(2).position.y = -164 - weaponOffset
	weapon1.get_child(3).position.y = -164 - weaponOffset
	#Weapon2 (Toaster)
	weapon2.get_child(0).position.x = 210 + weaponOffset
	weapon2.get_child(1).position.x = 220 + weaponOffset
	#Weapon3 (MG)
	weapon3.get_child(0).position.x = 166 + weaponOffset
	weapon3.get_child(2).position.x = 190 + weaponOffset
	
	#------Weapon1 (Kettensäge) Usage------
	if(inSpaceship and currentWeapon == 1):
		weapon1.modulate.a = 1.0
		if Input.is_action_just_pressed("use_weapon"):
			$"Weapon1/SägenSteamPlayer".play()
		if Input.is_action_just_released("use_weapon"):
			$"Weapon1/SägenSteamPlayer".stop()
		if(Input.is_action_pressed("use_weapon")):
			weapon1.get_child(2).emitting = true
			weapon1.get_child(3).get_child(0).disabled = false
			if(currentFrame % 2 == 0):
				weapon1.get_child(1).visible = !weapon1.get_child(1).visible
				for device in Input.get_connected_joypads():
					#device, Value Weak Motor, Value Strong Motor, Time - float zwischen 0 und 1 pls
					Input.start_joy_vibration(device, 0, 0.4, 0.05)
		else:
			weapon1.get_child(2).emitting = false
			weapon1.get_child(3).get_child(0).disabled = true
	else:
		weapon1.modulate.a = 0.6
	#------Weapon2 (Toaster) Usage------
	weapon2Cooldown += delta
	if(inSpaceship and currentWeapon == 2):
		weapon2.modulate.a = 1.0
		if(Input.is_action_just_pressed("use_weapon") and weapon2Cooldown >= weapon2CooldownLength):
			audioManager.playAudio(laser_shoot)
			for device in Input.get_connected_joypads():
				#device, Value Weak Motor, Value Strong Motor, Time - float zwischen 0 und 1 pls
				Input.start_joy_vibration(device, 0, 0.6, 0.5)
			weapon2Cooldown = 0.0
			weapon2.get_child(2).play("Weapon2Shoot")
			var allBodies = weapon2.get_child(1).get_overlapping_bodies()
			for body in allBodies:
				if(body.has_method("waveEnemy")):
					body.damage(weapon2Damage)
					body.electrify(weapon2ElectrificationLength)
					var allExistingEnemies = waveSpawner.get_children()
					allExistingEnemies.erase(body)
					for enemy in allExistingEnemies:
						var dist = body.global_position.distance_to(enemy.global_position)
						if(dist < weapon2JumpDistance):
							var chainLightInst = weapon2ChainLightning.instance()
							add_child(chainLightInst)
							chainLightInst.points[0] = body.global_position
							chainLightInst.points[1] = enemy.global_position
							enemy.damage(weapon2Damage)
							enemy.electrify(weapon2ElectrificationLength)
							audioManager.playAudio(rat_electric)
	else:
		weapon2.modulate.a = 0.6
	
	#------Weapon3 (MG) Usage------
	if(inSpaceship and currentWeapon == 3):
		weapon3.get_child(1).playback_speed = 5
		weapon3.modulate.a = 1.0
		if(Input.is_action_pressed("use_weapon") and weapon3Timer >= weapon3Cooldown):
			if randi() % 2:
				weapon3.get_child(1).play("Fire1")
			else:
				weapon3.get_child(1).play("Fire2")
			var shotProjectile = shotProjectileObj.instance()
			shotProjectile.damage = weapon3Damage
			shotProjectile.directionAngle = Vector2(cos(weapon3.rotation), sin(weapon3.rotation))
			shotProjectile.position = weapon3.get_child(2).global_position
			add_child(shotProjectile)
			audioManager.playAudio(shot)
			for device in Input.get_connected_joypads():
				#device, Value Weak Motor, Value Strong Motor, Time - float zwischen 0 und 1 pls
				Input.start_joy_vibration(device, 0, 0.6, 0.1)
			weapon3Timer = 0.0
			weapon3.get_child(1).queue("Idle")
	else:
		weapon3.modulate.a = 0.6
	#Bug Fixes für Sound und Particles der Säge
	if not inSpaceship or not currentWeapon == 1:
		$"Weapon1/SägenSteamPlayer".stop()
		weapon1.get_child(2).emitting = false
		weapon1.get_child(3).get_child(0).disabled = true
		
#--------Weapon Upgrade Funktionen--------
func weapon1DMGUp():
	weapon1Damage += 2
	print("Waffe1 Schaden UP: " + str(weapon1Damage))

func weapon2DMGUp():
	weapon2Damage += 2
	print("Waffe2 Schaden UP: " + str(weapon2Damage))

func weapon2ElectroLengthUP():
	weapon2ElectrificationLength += 0.2
	print("Waffe2 Electrification UP: " + str(weapon2ElectrificationLength))

func weapon2CooldownUP():
	#Berechnung verhindert, dass der Cooldown irgendwann zu niedrig wird und nicht mehr funktioniert
	var calculated = weapon2CooldownLength - 0.7
	calculated = clamp(calculated, 0.015, 1.5)
	weapon2CooldownLength -= calculated / 20.0
	print("Waffe2 Cooldown UP: " + str(weapon2CooldownLength))

func weapon3DMGUP():
	weapon3Damage += 2
	print("Waffe3 Schaden UP: " + str(weapon3Damage))

func weapon3CooldownUP():
	var calculated = weapon3Cooldown - 0.05
	calculated = clamp(calculated, 0.00015, 1.0)
	weapon3Cooldown -= calculated / 10.0
	print("Waffe2 Cooldown UP: " + str(weapon3Cooldown))

		
func playAudio(streamPlayer, sound):
	streamPlayer.stream = sound
	streamPlayer.play()
