extends Node2D

export(float) var maxHealth = 3000.0
var health = 3000.0
export(NodePath) var layer1
onready var layer1Inst = get_node(layer1)
export(NodePath) var layer2
onready var layer2Inst = get_node(layer2)
export(NodePath) var layer3
onready var layer3Inst = get_node(layer3)
export(NodePath) var shieldHealthDisplay
onready var shieldHealthDisplayInst = get_node(shieldHealthDisplay)
export(NodePath) var shipHealthDisplay
onready var shipHealthDisplayInst = get_node(shipHealthDisplay)
export(NodePath) var waveSpawner
onready var waveSpawnerInst = get_node(waveSpawner)
var reloadingTime = 0.0
var reloadingSpeed = 30.0
var activeLayers = 0

#Spaceship (Der Einfachheit halber mit hier im Schild untergebracht)
export(float) var maxShipHealth = 1500.0
var shipHealth = 1500.0
export(int) var lives = 3
export(NodePath) var spaceShip
onready var spaceShipInst = get_node(spaceShip)
export(NodePath) var livesLabel
onready var livesLabelInst = get_node(livesLabel)

#Audio
var audioShield1 = preload("res://Assets/Sound/Homeship/ovox_shieldDown1.mp3")
var audioShield2 = preload("res://Assets/Sound/Homeship/ovox_shieldDown2.mp3")
var playerHit = [
	preload("res://Assets/Sound/Player/player_hit.mp3"),
	preload("res://Assets/Sound/Player/player_hit2.mp3")
]
var firstDown = 0 #um den Sound nach jedem Schild abzuspielen
onready var audioManager = $"../../AudioManager"

func _ready():
	health = maxHealth
	shipHealth = maxShipHealth

func _process(delta):
	#Anhand der Ringe des Schildes anzeigen, wie viel Leben das Schild noch hat
	shieldHealthDisplayInst.value = 10 + (health / maxHealth) * 65.0
	updateVisibleLayer() #siehe unten
	if(shipHealth > 0): #or (lives > 0 and health == 0)
		reloadingTime += delta
		if(reloadingTime >= 10.0):
			health += delta * reloadingSpeed
		#Auch das Leben des Spaceships/Charakters regenerieren, aber später
		#und langsamer
		if(reloadingTime >= 18.0 and shipHealth < maxShipHealth):
			shipHealth += delta * reloadingSpeed / 12.0
	
	#Je nach Leben des SpaceShips den entsprechenden Frame anzeigen
	shipHealthDisplayInst.value = (shipHealth / maxShipHealth) * 100.0
	if(shipHealth == maxShipHealth):
		spaceShipInst.frame = 0
	elif(shipHealth >= 0.75 * maxShipHealth):
		spaceShipInst.frame = 1
	elif(shipHealth >= 0.50 * maxShipHealth):
		spaceShipInst.frame = 2
	elif(shipHealth >= 0.25 * maxShipHealth):
		spaceShipInst.frame = 3
	elif(shipHealth <= 0.0 * maxShipHealth):
		spaceShipInst.frame = 4
		
	livesLabelInst.text = str(lives)
	
	#Nur zu Debugzwecken
	#if(Input.is_action_just_pressed("tasteq")):
	#	damage(600.0)
	
	if(health == 0):
		layer1Inst.modulate.a = 50.0
	else:
		layer1Inst.modulate.a = lerp(layer1Inst.modulate.a, 1.0, delta * 20)
		layer2Inst.modulate.a = lerp(layer2Inst.modulate.a, 1.0, delta * 20)
		layer3Inst.modulate.a = lerp(layer3Inst.modulate.a, 1.0, delta * 20)
	
	#Sicherstellen, dass Leben nie größer als maximal möglich ist
	if(health > maxHealth):
		health = maxHealth

func damage(amount, direct=false):
	var beginActiveLayer = activeLayers
	reloadingTime = 0.0
	if(!direct):
		health -= amount
		#Controllershake
		Input.start_joy_vibration(0, 0, 0.6, 0.1)
		if(lives > 0):
			if(health < 0.0):
				health = 0.0
				waveSpawnerInst.endWave(true)
				UserInterface.CameraShake("big")
				lives -= 1
			print("Shield Health reduced to: ", health)
		else:
			if(health < 0.0):
				health = 0.0
				shipHealth -= amount
				if(shipHealth < 0.0):
					shipHealth = 0.0
					waveSpawnerInst.endWave(false)
					UserInterface.CameraShake("big")
		updateVisibleLayer()
		var endActiveLayers = activeLayers
		#Richtiges Audio abspielen, je nachdem welche Schildebene zerstört wurde
		if(beginActiveLayer != endActiveLayers):
			if(beginActiveLayer == 3):
				audioManager.playAudio(audioShield1)
				UserInterface.CameraShake("big")
			elif(beginActiveLayer == 2):
				audioManager.playAudio(audioShield2)
				UserInterface.CameraShake("big")
		
		#Aufleuchten, wenn DER Schild gehittet wird
		match activeLayers:
			1:
				layer1Inst.modulate.a = 30.0
			2:
				layer2Inst.modulate.a = 30.0
			3:
				layer3Inst.modulate.a = 30.0
				
	#Für direkten Schaden am Leben des Schiffs statt Schild
	else:
		shipHealth -= amount
		var rng = RandomNumberGenerator.new()
		rng.randomize()
		var picked = rng.randi_range(0, playerHit.size() - 1)
		audioManager.playAudio(playerHit[picked])
		if(shipHealth < 0.0):
			shipHealth = 0.0
			waveSpawnerInst.endWave(false)
			UserInterface.CameraShake("big")
			
func updateVisibleLayer():
	if(health >= maxHealth * 0.67):
		layer1Inst.visible = true
		layer2Inst.visible = true
		layer3Inst.visible = true
		activeLayers = 3
		$Shield_SB/CollisionShape2D.shape.radius = 135.0
	elif(health >= maxHealth * 0.33):
		layer1Inst.visible = true
		layer2Inst.visible = true
		layer3Inst.visible = false
		activeLayers = 2
		$Shield_SB/CollisionShape2D.shape.radius = 130.0
	elif(health > 0):
		layer1Inst.visible = true
		layer2Inst.visible = false
		layer3Inst.visible = false
		activeLayers = 1
		$Shield_SB/CollisionShape2D.shape.radius = 125.0
	else:
		layer1Inst.visible = true
		layer2Inst.visible = false
		layer3Inst.visible = false
		activeLayers = 1
		$Shield_SB/CollisionShape2D.shape.radius = 125
