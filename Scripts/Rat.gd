extends KinematicBody2D

var target_entity = null
var state = "idle"
var rot = 0
var targetPos
var health = 100.0
var dead = false
var damage = 70.0
var bounceStrength = 4.0
var moveVector = Vector2(1.0, 0.0)
var speed = 105.0

var playerInRange = false

#onready var start_pos = global_position

var state_list = []

onready var roamingController = $Roaming

func _ready():
	self.performanceMode()
	var bodies = $ActiveRange.get_overlapping_bodies()
	for body in bodies:
		if(body.name == "Player"):
			self.playerInRange = true
			self.activeMode()

func pick_random_state(state_list):
	state_list.shuffle()
	return state_list.pop_front() #Shufffled liste und nimmt den wert an erster stelle

func performanceMode():
	$Sprite.visible = false
	$Sight.monitoring = false
	$Aggro.monitoring = false
	self.set_collision_layer_bit(0, false)

func activeMode():
	$Sprite.visible = true
	$Sight.monitoring = true
	$Aggro.monitoring = true
	self.set_collision_layer_bit(0, true)

func _physics_process(delta):
	if(self.playerInRange):
		if(!dead):
			if state == "hunting":
				#rot = lerp(rot, global_position.angle_to_point(target_entity.global_position) + deg2rad(180), 1.3 * delta)
				moveVector = lerp(moveVector, (target_entity.global_position - global_position).normalized() * speed * delta, 2.3 * delta)
				rot = moveVector.angle()
				self.rotation = rot
				var collision = move_and_collide(moveVector) # Enemy targeted und chased den Spieler
				if collision:
					#var rotVec = Vector2(sin(rot), cos(rot)).normalized()
					moveVector = moveVector.bounce(collision.normal).normalized() * bounceStrength
					if(collision.collider.name == "Player"):
						if(collision.collider.shield.shipHealth > 0.0):
							collision.collider.shipDamage(damage)
		
			if state == "lost":
				move_and_slide(Vector2(cos(rot), sin(rot)) * 50) # langsame chase nachdem der Spieler verloren wurde
	
			if state == "idle":
				rot = 0
	
			if state == "roaming":
				rot = self.position.direction_to(targetPos).normalized().angle()
				self.rotation = rot
				move_and_slide(self.position.direction_to(targetPos) * 50)
	
			if roamingController.get_time_left() == 0 and state != "hunting":
				state = pick_random_state(["idle", "roaming"])
				if state == "roaming":
					targetPos = roamingController.update_target_position()
					rot = global_position.angle_to_point(targetPos) + deg2rad(180)
					self.rotation = rot
				roamingController.start_roaming_timer(rand_range(3, 5)) # Switch zwischen roaming und idle/ geht aus der "lost" Phase raus
	
			$AnimationPlayer.play("RatMovement")
		else:
			$CollisionShape2D.disabled = true
			modulate.a = lerp(modulate.a, 0.5, 0.4 * delta)
		
		if(UserInterface.WaveSpawner.gameOver):
			playerInRange = false

func _on_Sight_body_exited(body: Node): # Verliert den Player wenn die Sight verlassen wird
	if is_instance_valid(target_entity):
		target_entity = null
		state = "lost"


func _on_Aggro_body_entered(body: Node): # Greift den Player an wenn Aggro betreten wird
	if !is_instance_valid(target_entity):
		target_entity = body
		state = "hunting"

func damage(amount):
	health -= amount
	if(health < 0.0):
		health = 0.0
	if(!dead and health <= 0.0):
		dead = true
		$AnimationPlayer.play("RatDeath")

#Ist zum Erkennen von anderen Skripts, bitte nicht löschen!
func roamingEnemy():
	pass


func _on_ActiveRange_body_entered(body):
	if(body.name == "Player" and !UserInterface.WaveSpawner.gameOver):
		playerInRange = true
		self.activeMode()
	pass


func _on_ActiveRange_body_exited(body):
	if(body.name == "Player" and !UserInterface.WaveSpawner.gameOver):
		playerInRange = false
		self.performanceMode()
	pass
