extends KinematicBody2D

var spaceship
var spaceshipPos
var health = 100
export(float) var unitHealth = 100
var moveVector
export(float) var speed = 3000
var currentSpeed
var dead = false
export(String) var moveAnimation = "KrakenMovement"
export(String) var deathAnimation = "KrakenDeath"
export(PackedScene) var damageParticles
export(PackedScene) var shieldHitParticles
export(PackedScene) var deathParticles
export(PackedScene) var krakenProjectile
var shootTimer = 0.0
export(float) var shootInterval = 5.5
export(float) var damageDealt = 35.0
var deathTimer = 0.0
export(float) var distanceToSpaceship = 235.0
var startSizeMod = 0.1
var lerpSpeed = 0.003

#Electrification
var electrified = false
var electrificationLength = 10.0
var electrificationTimer = 0.0

#Audio
var krakenSound = [preload("res://Assets/Sound/Kraken/ovox_kraken_1.mp3"),
preload("res://Assets/Sound/Kraken/ovox_kraken_2.mp3"),
preload("res://Assets/Sound/Kraken/ovox_kraken_3.mp3"),
preload("res://Assets/Sound/Kraken/ovox_kraken_4.mp3"),
preload("res://Assets/Sound/Kraken/ovox_kraken_5.mp3"),
preload("res://Assets/Sound/Kraken/ovox_kraken_6.mp3"),
preload("res://Assets/Sound/Kraken/ovox_kraken_7.mp3"),
preload("res://Assets/Sound/Kraken/ovox_kraken_8.mp3"),
preload("res://Assets/Sound/Kraken/ovox_kraken_9.mp3"),
preload("res://Assets/Sound/Kraken/ovox_kraken_10.mp3")]
var kraken_hurt = [
	preload("res://Assets/Sound/Kraken/Kraken_hit_1.wav"),
	preload("res://Assets/Sound/Kraken/Kraken_hit_2.wav")
]
var playSound = preload("res://Assets/Sound/playAudio.tscn")
var rng = RandomNumberGenerator.new()
var krakenSoundTimer = 0.0


func _ready():
	#spaceshipPos = get_node(spaceship)
	moveVector = self.position.direction_to(spaceshipPos.position)
	health = unitHealth
	currentSpeed = speed
	
	scale = Vector2(startSizeMod, startSizeMod)
	modulate.a = 0.0
	
	#Place Kraken near Ship
	rng.randomize()
	var randomRot = rng.randf_range(0.0, 360.0)
	var spawnDistVariation = rng.randf_range(0.95, 1.05)
	position = Vector2(1.0,0.0).rotated(randomRot) * distanceToSpaceship * 1.5 * spawnDistVariation
	
	krakenSoundTimer = rng.randf_range(0.0, 3.5)

func _physics_process(delta):
	if(!dead):
		scale = lerp(scale, Vector2(1.0,1.0), lerpSpeed)
		modulate.a = lerp(modulate.a, 1.0, lerpSpeed)
		shootTimer += delta
		if(self.position.distance_to(spaceshipPos.position) > distanceToSpaceship):
			moveVector = lerp(moveVector, self.position.direction_to(spaceshipPos.position), 0.05)
			var collision = move_and_collide(moveVector * currentSpeed * delta)
		elif(scale.length() >= 0.93 and modulate.a >= 0.93):
			if(shootTimer >= shootInterval):
				shootTimer = 0.0
				var projectileInst = krakenProjectile.instance()
				var directionToShip = self.global_position.direction_to(spaceshipPos.position)
				projectileInst.position = global_position + directionToShip.normalized() * 15.0
				projectileInst.moveVector = directionToShip
				get_tree().root.add_child(projectileInst)
		if(moveAnimation != ""):
			$AnimationPlayer.play(moveAnimation)
		#damage(10.0 * delta)
		#Nur für Testzwecke:
		#if(Input.is_action_pressed("tasteq")):
			#damage(70 * delta)
		
		#Electrification
		if(electrified):
			electrificationTimer += delta
			if(electrificationTimer >= electrificationLength):
				electrified = false
			currentSpeed = speed / 3.0
			if randi() % 2:
				$Sprite.modulate.a = 20.0
			else:
				$Sprite.modulate.a = 1.0
		else:
			$Sprite.modulate.a = 1.0
			currentSpeed = speed
		
		#Play random Sounds
		krakenSoundTimer += delta
		if(krakenSoundTimer >= 8.5):
			krakenSoundTimer = rng.randf_range(0.0, 5.0)
			playRandomKrakenSound()
			
	else:
		$AnimationPlayer.stop()
		deathTimer += delta
		if(deathTimer < 0.3 and deathAnimation != ""):
			$AnimationPlayer.play(deathAnimation)
			$Sprite.modulate.a = 1.0
		if(deathTimer >= 5):
			$Sprite.modulate.a = lerp($Sprite.modulate.a, 0.2, 0.4 * delta)
		if(deathTimer >= 30):
			queue_free()

func damage(amount):
	if(modulate.a <= 0.80):
		return
	
	health -= amount
	if not electrified:
		var rng = RandomNumberGenerator.new()
		rng.randomize()
		var picked = rng.randi_range(0, kraken_hurt.size() - 1)
		playAudio(kraken_hurt[picked])
	if(!dead):
		var damageParticleInstance = damageParticles.instance()
		add_child(damageParticleInstance)
		damageParticleInstance.emitting = true
	if(health < 0):
		health = 0
	if(health == 0 && !dead):
		#if(deathParticles != null):
		#	var deathParticleInstance = deathParticles.instance()
		#	add_child(deathParticleInstance)
		#	deathParticleInstance.emitting = true
		for i in range(0, 8):
			var damageParticleInst = damageParticles.instance()
			add_child(damageParticleInst)
			damageParticleInst.emitting = true
		dead = true
		$CollisionShape2D.disabled = true
		$Sprite.modulate.a = 1.0

func electrify(length):
	electrified = true
	electrificationTimer = 0.0

func playRandomKrakenSound():
	rng.randomize()
	var randomChosen = rng.randi_range(0, krakenSound.size() - 1)
	playAudio(krakenSound[randomChosen])

#Wird gebraucht, um zu erkennen, ob ein Objekt dieses Skript hat, bitte nicht entfernen!
func waveEnemy():
	pass
	
func playAudio(audio):
	var sound = playSound.instance()
	add_child(sound)
	sound.playAudioFunc(audio)
