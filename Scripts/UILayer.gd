extends CanvasLayer

export(PackedScene) var messageBox
export(PackedScene) var infoBox
var currentInfoBox = null

class_name UILayer

func _ready():
	$SceneTransitionLayer/SceneTransitionBlack.modulate.a = 1
	$SceneTransitionLayer/AnimationPlayer.play("SceneTransition")
	UserInterface.Message = self

#Es kann von überall aus 
#	UserInterface.Message.UIMessage(message, duration) 
#aufgerufen werden, um eine Infobox über dem Spiel für die vorgegebene Dauer mit dem
#vorgegebenen Text erscheinen zu lassen.
func UIMessage(message, duration) -> void:
	var messageBoxInstance = messageBox.instance()
	add_child(messageBoxInstance)
	messageBoxInstance.get_child(0).text = message
	messageBoxInstance.messageDuration = duration
	

#Kann auch von überall aus aufgerufen werden mit
#	UserInterface.Message.UIInfoBoxShow(message)
#Darf auch öfter aufgerufen werden, führt zu keinen Problemen.
func UIInfoBoxShow(message) -> void:
	if(currentInfoBox == null):
		currentInfoBox = infoBox.instance()
		add_child(currentInfoBox)
		currentInfoBox.get_child(0).text = message
	else:
		print("Es existiert bereits eine Info Box!")

#Alternative Infobox, mit Duration, kann mit
#	UserInterface.Message.UIInfoBoxMessage(message, duration)
#aufgerufen werden.
func UIInfoBoxMessage(message, duration) -> void:
	if(currentInfoBox == null):
		currentInfoBox = infoBox.instance()
		add_child(currentInfoBox)
		currentInfoBox.get_child(0).text = message
		currentInfoBox.useDuration = true
		currentInfoBox.duration = duration
	else:
		print("Es existiert bereits eine Info Box!")

func UIInfoBoxHide() -> void:
	if(currentInfoBox != null):
		currentInfoBox.hideBox()
		currentInfoBox = null
