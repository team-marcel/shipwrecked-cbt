extends Node2D

export(PackedScene) var fireWorksParticles

func launchFireworks():
	var fireworksInst = fireWorksParticles.instance()
	add_child(fireworksInst)
